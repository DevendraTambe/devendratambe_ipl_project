const matchesJson = require('./Src/data/matchesJson.json');
const deliveriesJson = require('./Src/data/deliveriesJson.json');


//Q.1 No. of matches plyed per year.
let matches = {};
function matchesPlayedPerYear(matchData) {
    matchData.map(year => {
        if (matches[year.season] == null) {
            matches[year.season] = 1;
        } else {
            matches[year.season] = ++matches[year.season];
        }
    });
    return matches;
}


//Q.2 Matches won by per year.
function matchesPlayedWon(matchData) {
    let takeYears = {}
    matchData.map((idx1) => {
        if (!takeYears[idx1['season']]) {
            takeYears[idx1['season']] = {}

            let teamAndWins = {}
            matchData.map(idx2 => {
                if (idx2['season'] === idx1['season']) {
                    if (!teamAndWins[idx2['winner']]) {
                        teamAndWins[idx2['winner']] = 1;
                    } else {
                        teamAndWins[idx2['winner']]++
                    }
                }
            })
            takeYears[idx1['season']] = teamAndWins;
        }
    })
    return takeYears
}


// Q.3 Extra run given by team in 2016
function extraRuns(matchData, deliveriesJson) {
    let getMatchid = [];
    matchData.map(match => {
        if (match['season'] === '2016') {
            getMatchid.push(match['id']);
        }
    });
    let extraRuns = {};
        deliveriesJson.map(delivery => {
            if (getMatchid.includes(delivery['match_id'])) {
                if (extraRuns.hasOwnProperty([delivery['bowling_team']])) {
                    extraRuns[delivery['bowling_team']] =
                        parseInt(extraRuns[delivery['bowling_team']]) + parseInt(delivery['extra_runs']);
                } else {
                    extraRuns[delivery['bowling_team']] = parseInt(delivery['extra_runs']);
                }
            }
    });
    return extraRuns;
}

//Q.4 : Top 10 Economical bollers in 2015
function topBowler(matchData, deliveriesJson) {
    let matchids = [];
    matchData.map(match => {
        if (match['season'] === '2015') {
            matchids.push(match['id']);
        }
    });
    let bowlers = deliveriesJson.reduce(function (bowlerName, curr){
        if(matchids.includes(curr['match_id'])){
            if(!bowlerName[curr.bowler]){
                bowlerName[curr.bowler]=[];
                bowlerName[curr.bowler]['over']=1;
                bowlerName[curr.bowler]['runs']=parseInt(curr.total_runs);
            }else{
                bowlerName[curr.bowler]['over']+=1;
                bowlerName[curr.bowler]['runs']+=Number(curr.total_runs);
            }
        }
        return bowlerName
    }, {});
    let top10 = Object.entries(bowlers)
    .map(function (element) {
      let over = Math.floor(element[1]['over'] / 6);
      let boll = '0.' + String(element[1]['over'] % 6);
      let totalOver = over + Number(boll);
      return [          
        element[0],Number((element[1] = element[1]['runs'] / totalOver)).toFixed(2),
      ];
    })
    .sort((a, b) => a[1] - b[1])
    .slice(0, 10)
    .reduce(function (acc, curr) {
      if (!acc[curr[0]]) {
        acc[curr[0]] = curr[1];
      }
      return acc;
    }, {});
    
  return top10;
}

// Q5. Find the number of times each team won the toss and also won the match
function numberOfTeamWon(matches){
    let winners = matches.reduce(function(acc, match){
        if(!acc[match['toss_winner']]&&(acc[match['toss_winner']]===acc[match['winner']])){
            acc[match['toss_winner']] = 1;
        }else if(acc[match['toss_winner']]===acc[match['winner']]){
            acc[match['toss_winner']] = acc[match['toss_winner']]+1;
        }
        return acc;
    }, {});
    return winners;
}
/* let res = numberOfTeamWon(matchesJson);
console.log(res); */

//Q6. Highest No. of Player of the match
function playerOfTheMatch(matches) {

    let manOfMatch = matches.reduce((acc, match) => {
      if (acc.hasOwnProperty(match.season)) {
        if (acc[match.season].hasOwnProperty(match.player_of_match)) {
          acc[match.season][match.player_of_match] += 1;
        } else {
          acc[match.season][match.player_of_match] = 1;
        }
      }
      else {
        acc[match.season] = {};
      }
      return acc;
    }, {});
    
    let sortedAward = Object.values(manOfMatch)
        .map(nameNdCount => Object.entries(nameNdCount)
        .sort((a, b) => a[1] - b[1]));
  
    const topPlayers = sortedAward.map(nameAwards => nameAwards[nameAwards.length-1]);
   
    let awardsPlayer =  matches.reduce((acc, match) => {
      acc[match.season] = 0;
      return acc;
  }, {});
  
    
    Object.keys(awardsPlayer).forEach((element,index) => {
      let name = topPlayers[index][0];
      let value = topPlayers[index][1];
      awardsPlayer[element] = { [name]: value }
    })
   
    return awardsPlayer;
  }
  
//Q.7 strike rate of a batsman for each season
function strikeRate(deliveries) {
	const runScoredByBatsmen = {};
	const ballPlayedByBatsmen = {};
	deliveries.forEach((deliverie) => {
		let match_id = deliverie.match_id;
		let batsman = deliverie.batsman;
		let batsman_runs = deliverie.batsman_runs;
		let batsman_faced = deliverie.ball;
		if (parseInt(match_id) >= 1 && parseInt(match_id) <= 59) {
			let season = 2017;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 60 && parseInt(match_id) <= 117) {
			let season = 2008;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 118 && parseInt(match_id) <= 174) {
			let season = 2009;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 175 && parseInt(match_id) <= 234) {
			let season = 2010;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 235 && parseInt(match_id) <= 307) {
			let season = 2011;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 308 && parseInt(match_id) <= 381) {
			let season = 2012;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 382 && parseInt(match_id) <= 457) {
			let season = 2013;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 458 && parseInt(match_id) <= 517) {
			let season = 2014;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 518 && parseInt(match_id) <= 576) {
			let season = 2015;
			runScored(season);
			ballsPlayed(season);
		}
		if (parseInt(match_id) >= 577 && parseInt(match_id) <= 636) {
			let season = 2016;
			runScored(season);
			ballsPlayed(season);
		}
		function runScored(season) {
			if (runScoredByBatsmen[season] === undefined) {
				runScoredByBatsmen[season] = {};
			}
			if (runScoredByBatsmen[season][batsman] !== undefined) {
				runScoredByBatsmen[season][batsman] += parseInt(batsman_runs);
			} else {
				runScoredByBatsmen[season][batsman] = parseInt(batsman_runs);
			}
		}
		function ballsPlayed(season) {
			if (ballPlayedByBatsmen[season] === undefined) {
				ballPlayedByBatsmen[season] = {};
			}
			if (ballPlayedByBatsmen[season][batsman] !== undefined) {
				if (batsman_faced !== undefined)
					ballPlayedByBatsmen[season][batsman] += 1;
			} else {
				ballPlayedByBatsmen[season][batsman] = 1;
			}
		}
	});
	let strikeRateObj = {};
    let strikeRateOfBatsman=[];
	getStrikeRate(runScoredByBatsmen, ballPlayedByBatsmen);
     //console.log(strikeRateOfBatsman);
	function getStrikeRate(runScoredByBatsmen, ballPlayedByBatsmen) {
		for (let runScoredYear in runScoredByBatsmen) {
			for (let runScoredYearName in runScoredByBatsmen[runScoredYear]) {
				let run = runScoredByBatsmen[runScoredYear][runScoredYearName];
				let ball = ballPlayedByBatsmen[runScoredYear][runScoredYearName];
				let strickRateBatsman = ((run / ball) * 100).toFixed(2);
				if (strickRateBatsman != 0) {
					strikeRateObj[runScoredYear] = {[runScoredYearName]: strickRateBatsman,};
                    strikeRateOfBatsman.push(runScoredYear, strikeRateObj[runScoredYear])
				}
                
			}
		}
	}    
	return strikeRateObj;
}
strikeRate(deliveriesJson);

//Q.8 Dissmseled Player
function dismmseledPlayer(delivery){
    let dismmseledPlayer = delivery.reduce((acc, match)=>{
        if(match.player_dismissed){
            let playerDissmiss = match.player_dismissed;
            let dissmissedPlayerBy = match.fielder;
            if(acc.hasOwnProperty(playerDissmiss)){
                if(acc[playerDissmiss].hasOwnProperty(dissmissedPlayerBy)){
                    acc[playerDissmiss][dissmissedPlayerBy]['count']=parseInt(acc[playerDissmiss][dissmissedPlayerBy]['count'])+1;
                }else{
                    acc[playerDissmiss][dissmissedPlayerBy]={};
                    acc[playerDissmiss][dissmissedPlayerBy]['count']=1;
                }
            }else{
                acc[playerDissmiss]={};
                acc[playerDissmiss][dissmissedPlayerBy]={};
                acc[playerDissmiss][dissmissedPlayerBy]['count']=1;
            }
        }
        return acc;
    },{});
    return dismmseledPlayer;
}

function ballwerBestEconomy(delivery){
    let ballersWithEconomy = delivery.reduce((acc, match)=>{
        if(acc.hasOwnProperty(match.bowler)){
            if(match.is_super_over==='1'){
                acc[match.bowler].balls=parseInt(acc[match.bowler].balls)+1;
                acc[match.bowler].runs=parseInt(acc[match.bowler].runs)+parseInt(match.total_runs)
                let run = parseInt(acc[match.bowler].runs);
                let ball = parseInt(acc[match.bowler].balls);
                acc[match.bowler].economy = (run/(ball/6).toFixed(2));
            }
        }
        else{
            if(match.is_super_over==='1'){
                acc[match.bowler] = {'runs':match.total_runs, 'balls':1, 'economy':0};
            }
        }
        return acc;
    },{});
    
    let economies = Object.values(ballersWithEconomy).map((bowllerEconomy)=>parseFloat(bowllerEconomy.economy));

    let topEconomies = economies.filter((bollwer)=>{
        if(parseInt(bollwer)<10){
            return bollwer;
        }
    });

    topEconomies.sort();

    const topBowller={};

    for(let key in ballersWithEconomy){
        if(ballersWithEconomy[key]['economy']==topEconomies[0]){
            topBowller[key]=topEconomies[0];
        }
    }
    
    return topBowller;
}
module.exports = {matchesPlayedPerYear, matchesPlayedWon, extraRuns, topBowler, numberOfTeamWon, 
    playerOfTheMatch, strikeRate, dismmseledPlayer,ballwerBestEconomy};
const csvtojson = require('csvtojson');
const fs = require('fs');
const ipl = require('Src/server/ipl.js');

const matchesJson = require('../Src/data/matchesJson.json');
const deliveriesJson = require('../Src/data/deliveriesJson.json');

const matchesFilePath = './Src/data/matches.csv';
const deliveriesFilePath = './Src/data/deliveries.csv';

 csvtojson()
    .fromFile(matchesFilePath)
    .then((json) => {
        //console.log(json);
        fs.writeFileSync("Src/data/matchesJson.json",
            JSON.stringify(json), "utf-8", (err) => {
                if (err) {
                    console.log("Error In Converting to matchesJson: " + err);
                }
            })
    });

csvtojson()
    .fromFile(deliveriesFilePath)
    .then((json) => {
        //console.log(json);
        fs.writeFileSync("Src/data/deliveriesJson.json",
            JSON.stringify(json), "utf-8", (err) => {
                if (err) {
                    console.log("Error In Converting to deliveriesJson: " + err);
                }
            })
    });
let outputMatchesPerYear = ipl.matchesPlayedPerYear(matchesJson);
fs.writeFileSync("Src/public/Output/matchesPerYear.json",
    JSON.stringify(outputMatchesPerYear), (error) => {
        if (error) {
            console.log("Error In Matches Per Year: " + error);
        }
});

let outputMatchesWon = ipl.matchesPlayedWon(matchesJson);
fs.writeFileSync("Src/public/Output/matchesWon.json",
JSON.stringify(outputMatchesWon),(error)=>{
    if(error){
        console.log("Error In Mathces Won: "+error);
    }
})


let outputExtraRuns = ipl.extraRuns(matchesJson,deliveriesJson);
fs.writeFileSync("Src/public/Output/extraRuns.json",
JSON.stringify(outputExtraRuns),(error)=>{
    if(error){
        console.log("Error In Extra Runs: " + error);
    }
});


let outputTopBollower = ipl.topBowler(matchesJson, deliveriesJson);
fs.writeFileSync("Src/public/Output/topBollower.json",
JSON.stringify(outputTopBollower),(error)=>{
    if(error){
        console.log("Error In Top Bollower: " + error);
    }
});

let outputNumberOfTeamWon = ipl.numberOfTeamWon(matchesJson);
fs.writeFileSync("Src/public/Output/numberOfTeamWon.json",
JSON.stringify(outputNumberOfTeamWon),(error)=>{
    if(error){
        console.log("Error In NumberOfTeamWon : " + error);
    }
});

let outputplayerOfTheMatch = ipl.playerOfTheMatch(matchesJson);
fs.writeFileSync("Src/public/Output/playerOfTheMatch.json",
JSON.stringify(outputplayerOfTheMatch),(error)=>{
    if(error){
        console.log("Error In playerOfTheMatch : " + error);
    }
});

let outputdismmseledPlayer = ipl.dismmseledPlayer(deliveriesJson);
fs.writeFileSync("Src/public/Output/dismmseledPlayer.json",
JSON.stringify(outputdismmseledPlayer),(error)=>{
    if(error){
        console.log("Error In dismmseledPlayer : " + error);
    }
});

let outputballwerBestEconomy = ipl.ballwerBestEconomy( deliveriesJson);
fs.writeFileSync("Src/public/Output/ballwerBestEconomy.json",
JSON.stringify(outputballwerBestEconomy),(error)=>{
    if(error){
        console.log("Error In ballwerBestEconomy : " + error);
    }
}); 


let outputstrikeRate = ipl.strikeRate(deliveriesJson);
fs.writeFileSync("Src/public/Output/strikeRate.json",
JSON.stringify(outputstrikeRate),(error)=>{
    if(error){
        console.log("Error In strikeRate: " + error);
    }
});